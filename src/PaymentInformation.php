<?php
/**
 * User: pavel
 * Date: 26.12.2016
 * Time: 20:43
 */

namespace PavelTizek\Invoice;


use Nette\SmartObject;

class PaymentInformation 
{

    use SmartObject;
    /** @var  string */
    private $currency;

    /** @var  string|null */
    private $variableSymbol;

    /** @var  string|null */
    private $constantSymbol;

    /** @var  float|null */
    private $tax;

    /** @var  string */
    private $paymentMethod;

    /**
     * PaymentInformation constructor.
     * @param $currency
     * @param $paymentMethod
     * @param null $variableSymbol
     * @param null $constantSymbol
     * @param null $tax
     */
    public function __construct($currency, $paymentMethod, $variableSymbol = null, $constantSymbol = null, $tax = NULL)
    {
        $this->currency = $currency;
        $this->paymentMethod = $paymentMethod;
        $this->variableSymbol = $variableSymbol;
        $this->constantSymbol = $constantSymbol;
        $this->tax = $tax;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return null|string
     */
    public function getVariableSymbol()
    {
        return $this->variableSymbol;
    }

    /**
     * @return null|string
     */
    public function getConstantSymbol()
    {
        return $this->constantSymbol;
    }

    /**
     * @return float|null
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }






}
