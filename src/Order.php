<?php
/**
 * User: pavel
 * Date: 26.12.2016
 * Time: 20:01
 */

namespace PavelTizek\Invoice;



use Nette\SmartObject;

class Order 
{

    use SmartObject;
    /** @var  string|int */
    private $number;

    /** @var  \DateTime */
    private $dueDate;

    /** @var  \DateTime */
    private $created;

    /** @var  Account */
    private $account;

    /** @var  PaymentInformation */
    private $paymentInformation;

    /** @var  Item[] */
    private $items = [];

    /** @var  string */
    private $contractNumber;


    /**
     * Order constructor.
     * @param $number
     * @param \DateTime $dueDate
     * @param \DateTime $created
     * @param Account $account
     * @param PaymentInformation $paymentInformation
     * @param null $contractNumber
     */
    public function __construct($number, \DateTime $dueDate, \DateTime $created, Account $account, PaymentInformation $paymentInformation, $contractNumber = null)
    {
        $this->number = $number;
        $this->dueDate = $dueDate;
        $this->created = $created;
        $this->account = $account;
        $this->paymentInformation = $paymentInformation;
        $this->contractNumber = $contractNumber;
    }


    /**
     * @param Item $item
     */
    public function addItem(Item $item){
        $this->items[] = $item;
    }

    /**
     * @return int|string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return PaymentInformation
     */
    public function getPaymentInformation()
    {
        return $this->paymentInformation;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return float|int
     */
    public function getTotal(){
        $total = 0;

//        if ($this->paymentInformation->getTax() == 0 || ){
//            $tax = 0;
//        }
//        else{
//            $tax = 1;
//        }
        foreach ($this->items as $item) {
            $total += $item->getPrice() * $item->getCount();
        }
        return $total;
    }



}
