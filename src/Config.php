<?php
/**
 * User: pavel
 * Date: 29.12.2016
 * Time: 11:54
 */

namespace PavelTizek\Invoice;


use Nette\SmartObject;

class Config 
{

    use SmartObject;
    /** @var  string */
    private $name;

    /** @var  string */
    private $footerText;

    /**
     * Config constructor.
     * @param string $name
     * @param string $footerText
     */
    public function __construct($name, $footerText)
    {
        $this->name = $name;
        $this->footerText = $footerText;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFooterText()
    {
        return $this->footerText;
    }

    /**
     * @param string $footerText
     */
    public function setFooterText($footerText)
    {
        $this->footerText = $footerText;
    }




}
