<?php
/**
 * User: pavel
 * Date: 26.12.2016
 * Time: 20:48
 */

namespace PavelTizek\Invoice;


use Nette\Localization\ITranslator;

class Translator implements ITranslator
{

    const ENGLISH = 'en';
    const CZECH = 'cs';
    const SLOVAK = 'sk';

    /** @var  string */
    private $lang;


    /** @var array */
    private static $translations = [
        'cs' => [
            'customer' => 'Zákazník',
            'company' => 'Společnost',
            'invoice' => 'Faktura',
            'created' => 'Vytvořeno',
            'due' => 'Splatnost',
            'item' => 'Položka',
            'price' => 'Cena',
            'total' => 'Celkem',
            'count' => 'Položek',
            'price_tax' => 'Cena s DPH',
            'price_unit' => 'Cena za jednotku',
            'tax' => 'DPH',
            'cz' => 'Česká Republika',
            'sk' => 'Slovensko',
            'contract_number' => 'Číslo smlouvy',
            'supplier' => 'Dodavatel',
            'subscriber' => 'Odběratel',
            'tin' => 'IČ',
            'va_tin' => 'DIČ',
            'vat_payer' => 'Plátce DPH',
            'vat_not_payer' => 'Neplátce DPH',
            'bank_account' => 'Bankovní spojení',
            'bank_account_number' => 'Číslo účtu',
            'swift' => 'SWIFT',
            'iban' => 'iBan',
            'variable_symbol' => 'Variabilní symbol',
            'payment_method' => 'Způsob platby',
            'date_of_issue' => 'Datum vystavení',
            'due_date' => 'Datum splatnosti',
            'taxable_date' => 'Datum zdanitelného plnění',
            'paid_from_request' => 'Neplaťte, zaplaceno z výzvy',
            'amount' => 'Množství',
            'unit_price' => 'Cena za MJ',
            'sum' => 'Součet',
            'round' => 'Zaokrouhlení',
            'total_to_pay' => 'Celkem k úhradě',
            'vat_recap' => 'Rekapitulace DPH',
            'rate' => 'Sazba',
            'tax_base' => 'Základ daně',
            'total_vat' => 'Celkem s DPH',
            'customer_pay_vat' => 'Daň odvede zákazník, místem plnění pro účely DPH je sídlo příjemce služby.',
            'exposed' => 'Vystavil'


        ],

        'sk' => [
            'customer' => 'Zákazník',
            'company' => 'Společnost',
            'invoice' => 'Faktura',
            'created' => 'Vytvořeno',
            'due' => 'Splatnost',
            'item' => 'Položka',
            'price' => 'Cena',
            'total' => 'Celkem',
            'count' => 'Položek',
            'price_tax' => 'Cena s DPH',
            'price_unit' => 'Cena za jednotku',
            'tax' => 'DPH',
            'cz' => 'Česká Republika',
            'sk' => 'Slovensko',
            'contract_number' => 'Číslo smlouvy',
            'supplier' => 'Dodavatel',
            'subscriber' => 'Odběratel',
            'tin' => 'IČ',
            'va_tin' => 'DIČ',
            'vat_payer' => 'Plátce DPH',
            'vat_not_payer' => 'Neplátce DPH',
            'bank_account' => 'Bankovní spojení',
            'bank_account_number' => 'Číslo účtu',
            'swift' => 'SWIFT',
            'iban' => 'iBan',
            'variable_symbol' => 'Variabilní symbol',
            'payment_method' => 'Způsob platby',
            'date_of_issue' => 'Datum vystavení',
            'due_date' => 'Datum splatnosti',
            'taxable_date' => 'Datum zdanitelného plnění',
            'paid_from_request' => 'Neplaťte, zaplaceno z výzvy',
            'amount' => 'Množství',
            'unit_price' => 'Cena za MJ',
            'sum' => 'Součet',
            'round' => 'Zaokrouhlení',
            'total_to_pay' => 'Celkem k úhradě',
            'vat_recap' => 'Rekapitulace DPH',
            'rate' => 'Sazba',
            'tax_base' => 'Základ daně',
            'total_vat' => 'Celkem s DPH',
            'customer_pay_vat' => 'Daň odvede zákazník, místem plnění pro účely DPH je sídlo příjemce služby.',
            'exposed' => 'Vystavil'

        ],
        'en' => [
            'customer' => 'Customer',
            'company' => 'Company',
            'invoice' => 'Invoice',
            'created' => 'Created',
            'due' => 'Due',
            'item' => 'Item',
            'price' => 'Price',
            'total' => 'Total',
            'count' => 'Count',
            'price_tax' => 'Tax Price',
            'price_unit' => 'Unit price',
            'tax' => 'Tax',
            'cz' => 'Czech Republic',
            'sk' => 'Slovakia',
            'contract_number' => 'Contract Number',
            'supplier' => 'Supplier',
            'subscriber' => 'Subscriber',
            'tin' => 'IČ',
            'va_tin' => 'DIČ',
            'vat_payer' => 'Plátce DPH',
            'vat_not_payer' => 'Neplátce DPH',
            'bank_account' => 'Bankovní spojení',
            'bank_account_number' => 'Číslo účtu',
            'swift' => 'SWIFT',
            'iban' => 'iBan',
            'variable_symbol' => 'Variabilní symbol',
            'payment_method' => 'Způsob platby',
            'date_of_issue' => 'Datum vystavení',
            'due_date' => 'Datum splatnosti',
            'taxable_date' => 'Datum zdanitelného plnění',
            'paid_from_request' => 'Neplaťte, zaplaceno z výzvy',
            'amount' => 'Množství',
            'unit_price' => 'Cena za MJ',
            'sum' => 'Součet',
            'round' => 'Zaokrouhlení',
            'total_to_pay' => 'Celkem k úhradě',
            'vat_recap' => 'Rekapitulace DPH',
            'rate' => 'Sazba',
            'tax_base' => 'Základ daně',
            'total_vat' => 'Celkem s DPH',
            'customer_pay_vat' => 'Daň odvede zákazník, místem plnění pro účely DPH je sídlo příjemce služby.',
            'exposed' => 'Vystavil'

        ]
    ];


    /**
     * Translator constructor.
     * @param string $lang
     * @throws InvoiceException
     */
    public function __construct($lang = self::CZECH)
    {
        $this->lang = $lang;
        if (!isset(self::$translations[$this->lang])) {
            throw new InvoiceException("Language $lang not exists.");
        }
    }


    /**
     * @param string $message
     * @return string
     */
    public function translate($message, $count = null)
    {
        return self::$translations[$this->lang][$message];
    }
}
