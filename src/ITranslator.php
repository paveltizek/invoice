<?php
/**
 * User: pavel
 * Date: 26.12.2016
 * Time: 20:49
 */

namespace PavelTizek\Invoice;


interface ITranslator
{
    /**
     * @param string $message
     * @return string
     */
    public function translate($message);

}
