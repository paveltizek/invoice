<?php
/**
 * User: pavel
 * Date: 26.12.2016
 * Time: 19:58
 */

namespace PavelTizek\Invoice;


use Nette\SmartObject;

class Company 
{

    use SmartObject;
    /** @var  string */
    private $name;

    /** @var  string */
    private $street;

    /** @var  string */
    private $city;

    /** @var  string */
    private $zip;

    /** @var  string */
    private $country;

    /** @var  bool */
    private $hasTax;

    /** @var  string|null */
    private $tin;

    /** @var  string|null */
    private $vaTin;

    /** @var  string */
    private $logo;

    /** @var  string */
    private $accountNumber;

    /** @var  string */
    private $bank;

    /**
     * Company constructor.
     * @param string $name
     * @param string $street
     * @param string $city
     * @param string $zip
     * @param string $country
     * @param string $accountNumber
     * @param string $bank
     * @param bool $hasTax
     * @param null|string $tin
     * @param null|string $vaTin
     */
    public function __construct($name, $street, $city, $zip, $country, $accountNumber, $bank, $hasTax = FALSE, $tin = NULL, $vaTin = NULL)
    {
        $this->name = $name;
        $this->street = $street;
        $this->city = $city;
        $this->zip = $zip;
        $this->country = $country;
        $this->accountNumber = $accountNumber;
        $this->bank = $bank;
        $this->hasTax = $hasTax;
        $this->tin = $tin;
        $this->vaTin = $vaTin;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return bool
     */
    public function isHasTax()
    {
        return $this->hasTax;
    }

    /**
     * @return null|string
     */
    public function getTin()
    {
        return $this->tin;
    }

    /**
     * @return null|string
     */
    public function getVaTin()
    {
        return $this->vaTin;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @return string
     */
    public function getBank()
    {
        return $this->bank;
    }








}
