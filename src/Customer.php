<?php
/**
 * User: pavel
 * Date: 26.12.2016
 * Time: 19:57
 */

namespace PavelTizek\Invoice;


use Nette\SmartObject;

class Customer 
{

    use SmartObject;
    /** @var  string */
    private $name;

    /** @var  string */
    private $company;

    /** @var  string */
    private $street;

    /** @var  string */
    private $city;

    /** @var  string */
    private $zip;

    /** @var  string */
    private $country;

    /** @var  string */
    private $tin;

    /** @var  string */
    private $vaTin;

    /**
     * Customer constructor.
     * @param string $name
     * @param string $company
     * @param string $street
     * @param string $city
     * @param string $zip
     * @param string $country
     * @param string $tin
     * @param string $vaTin
     */
    public function __construct($name, $company, $street, $city, $zip, $country, $tin, $vaTin)
    {
        $this->name = $name;
        $this->company = $company;
        $this->street = $street;
        $this->city = $city;
        $this->zip = $zip;
        $this->country = $country;
        $this->tin = $tin;
        $this->vaTin = $vaTin;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getTin()
    {
        return $this->tin;
    }

    /**
     * @return string
     */
    public function getVaTin()
    {
        return $this->vaTin;
    }




}
