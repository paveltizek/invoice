<?php
/**
 * User: pavel
 * Date: 26.12.2016
 * Time: 20:32
 */

namespace PavelTizek\Invoice;


use Nette\SmartObject;

class Account 
{

    use SmartObject;

    /** @var  string */
    private $accountNumber;

    /** @var  string|null */
    private $iBan;

    /** @var  string|null */
    private $swift;

    /**
     * Account constructor.
     * @param string $accountNumber
     * @param null|string $iBan
     * @param null|string $swift
     */
    public function __construct($accountNumber, $iBan = null, $swift = null)
    {
        $this->accountNumber = $accountNumber;
        $this->iBan = $iBan;
        $this->swift = $swift;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @return null|string
     */
    public function getIBan()
    {
        return $this->iBan;
    }

    /**
     * @return null|string
     */
    public function getSwift()
    {
        return $this->swift;
    }




}
