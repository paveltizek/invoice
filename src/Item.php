<?php
/**
 * User: pavel
 * Date: 26.12.2016
 * Time: 20:01
 */

namespace PavelTizek\Invoice;


use Nette\SmartObject;

class Item 
{

    use SmartObject;
    /** @var  string */
    private $name;

    /** @var  int */
    private $count;

    /** @var  double */
    private $price;

    /**
     * Item constructor.
     * @param string $name
     * @param int $count
     * @param float $price
     */
    public function __construct($name, $count, $price)
    {
        $this->name = $name;
        $this->count = $count;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }




}
