<?php
/**
 * User: pavel
 * Date: 26.12.2016
 * Time: 19:57
 */

namespace PavelTizek\Invoice;

use Joseki\Application\Responses\PdfResponse;
use Latte\Engine;
use Latte\Runtime\Template;
use Nette\Localization\ITranslator;
use Nette\Templating\FileTemplate;
use Nette\Utils\DateTime;


use Nette\SmartObject;

class Invoice 
{

    use SmartObject;

    /** @var  string */
    private $form;

    /** @var  Customer */
    private $customer;

    /** @var  Company */
    private $company;

    /** @var  Order */
    private $order;

    /** @var  PdfResponse */
    private $pdf;

    /** @var  string */
    private $number;

    /** @var  Translator */
    private $translator;

    /** @var  \Nette\Bridges\ApplicationLatte\Template */
    private $template;

    /** @var  \Nette\Bridges\ApplicationLatte\Template */
    private $header;

    /** @var  \Nette\Bridges\ApplicationLatte\Template */
    private $footer;

    /** @var  DateTime */
    private $taxableDate;

    /** @var  Config */
    private $config;

    /** @var  bool */
    private $paidFromRequest;


    /**
     * Invoice constructor.
     * @param $form
     * @param Customer $customer
     * @param Company $company
     * @param Order $order
     * @param Config $config
     * @param $number
     * @param $paidFromRequest
     * @param null $date
     * @param $pricesWithVat
     * @param ITranslator|null $translator
     */
    public function __construct($form, Customer $customer, Company $company, Order $order, Config $config, $number, $paidFromRequest, $date = null, $pricesWithVat = false, ITranslator $translator = null)
    {
        $latte = new Engine();

        $this->template = new \Nette\Bridges\ApplicationLatte\Template($latte);
        $this->template->setFile(__DIR__ . '/../templates/inv.latte');
//        $this->header = new \Nette\Bridges\ApplicationLatte\Template($latte);
//        $this->header->setFile(__DIR__.'/../templates/header.latte');
        $this->template->addFilter('money', function ($number) {

            return $number . " " . $this->order->paymentInformation->currency;

        });

        $this->template->addFilter('plus_tax', function ($number) use ($pricesWithVat) {
            if (!$pricesWithVat) {
               
                return $number;
            } else {

                return $number * ($this->order->paymentInformation->getTax() + 1);
            }

        });

        $this->template->addFilter('round_up', function ($number) {
            return round(round($number, 1, PHP_ROUND_HALF_UP), 0, PHP_ROUND_HALF_UP);
        });

        $this->template->addFilter('tax', function ($number) {
            return $number * ($this->order->paymentInformation->getTax());
        });

        $this->template->addFilter('price', function ($number) use ($pricesWithVat) {
            if ($pricesWithVat) {

                return $number;
            } else {
                return $number / (($this->order->paymentInformation->getTax()*100) / 100 + 1);
            }
        });

        $this->template->addFilter('percent', function ($text) {
            return $text . ' %';
        });

        $this->template->addFilter('rest', function ($number) {
            $round = round(round($number, 0, PHP_ROUND_HALF_UP) - $number, 2);
            $temp = $round;
            $round = number_format($round, 2, ',', ' ');
            if ($temp > 0) {
                $round = '+' . $round;
            }
            return $round;
        });


        $this->customer = $customer;
        $this->company = $company;
        $this->order = $order;
        $this->config = $config;
        $this->number = $number;
        $this->form = $form;
        $this->taxableDate = $date;
        $this->paidFromRequest = $paidFromRequest;
        $this->translator = $translator == null ? new Translator() : $translator;
//        $this->pdf = null;

    }


    /**
     *
     */
    public function create()
    {
        $latte = new Engine();

        $this->pdf = new PdfResponse($this->template);


        $this->template->invoice = $this;
        $this->template->customer = $this->customer;
        $this->template->company = $this->company;
        $this->template->order = $this->order;
        $this->template->number = $this->number;
        $this->template->date = $this->taxableDate;

        $this->template->setTranslator($this->translator);

//        $this->header->invoice = $this;
//        $this->header->customer = $this->customer;
//        $this->header->company = $this->company;
//        $this->header->setTranslator($this->translator);

        $this->pdf->pageFormat = 'A4-P';
//        $this->pdf->pageMargins = '120,11,40,11,3,2';
        $this->pdf->setDocumentTitle($this->number);
        $this->pdf->styles = file_get_contents(__DIR__ . '/../res/css/invoice-print.css');

//        $this->pdf->getMPDF()->setHeader($this->header->getLatte()->renderToString(__DIR__.'/../templates/header.latte'));
//        $this->pdf->getMPDF()->setHTMLHeader($this->header);


    }

    /**
     * @return PdfResponse
     */
    public function getPdf()
    {
        return $this->pdf;
    }


    /**
     * @param $templateFile
     */
    public function setTemplate($templateFile)
    {
        $this->template->setFile($templateFile);

    }

    /**
     * @param string $path
     * @param string|null $fileName
     */
    public function save($path, $fileName = null)
    {
        $this->pdf->save($path, $fileName);
    }

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return DateTime
     */
    public function getTaxableDate()
    {
        return $this->taxableDate;
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return bool
     */
    public function isPaidFromRequest()
    {
        return $this->paidFromRequest;
    }


}
